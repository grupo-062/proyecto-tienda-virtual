DROP DATABASE IF EXISTS tiendavirtual06o25;

CREATE DATABASE IF NOT EXISTS tiendavirtual06O25;

USE tiendavirtual06O25;

DROP TABLE IF EXISTS usuarios;
DROP TABLE IF EXISTS tiendas;
DROP TABLE IF EXISTS productos;
DROP TABLE IF EXISTS movimientosdeinventarios;
DROP TABLE IF EXISTS comprobantes;
DROP TABLE IF EXISTS personas;

CREATE TABLE usuarios ( 
ID_Usuario INT NOT NULL AUTO_INCREMENT,
cedula_usuario INT NOT NULL,
Usuario	VARCHAR (20) NOT NULL,
nombre_completo_usuario VARCHAR (50) NOT NULL,
fecha_nacimiento_usuario DATE,
correo	VARCHAR (30) NOT NULL,
clave VARCHAR (20) NOT NULL,
activo BOOLEAN DEFAULT FALSE, 
primary key(ID_Usuario, Usuario, cedula_usuario))ENGINE =Innodb;


CREATE TABLE tiendas ( 
ID_Tienda INT NOT NULL AUTO_INCREMENT,
ID_Usuario INT NOT NULL,
nombre_Tienda VARCHAR (15) NOT NULL,
tipo_Tienda VARCHAR (10) NOT NULL,
direccion_Tienda VARCHAR (20) NOT NULL,
primary key(ID_Tienda),
foreign key (ID_Usuario) references usuarios (ID_Usuario))
ENGINE =Innodb;


CREATE TABLE productos (
ID_Producto INT AUTO_INCREMENT,  
Ref_Producto INT(5) NOT NULL,
Ref_Familia INT (2) NOT NULL,
ID_Tienda INT NOT NULL,
nombre_Producto VARCHAR (15) NOT NULL,
marca_Producto VARCHAR (15) NOT NULL,
nombre_Familia VARCHAR (15) NOT NULL,
cantidad DOUBLE NOT NULL,
unidad_Medida VARCHAR (10) NOT NULL,
precio_Producto DOUBLE NOT NULL,
primary key(ID_Producto),
foreign key (ID_Tienda) references tiendas (ID_Tienda))
ENGINE =Innodb;


CREATE TABLE IF NOT EXISTS movimientosdeinventarios(
ID_Movimientos INT NOT NULL AUTO_INCREMENT,
ID_Usuario INT NOT NULL, 
ID_tipomovimiento VARCHAR (1) NOT NULL, 
ID_Producto INT NOT NULL, 
fecha DATE, 
cantidad INT NOT NULL DEFAULT 0, 
anulado BOOLEAN, PRIMARY KEY (ID_Movimientos), 
FOREIGN KEY (ID_Usuario) REFERENCES usuarios(ID_Usuario),
FOREIGN KEY (ID_Producto) REFERENCES productos(ID_Producto)) ENGINE = InnoDB;



CREATE TABLE IF NOT EXISTS comprobantes(
ID_Comprobante INT NOT NULL AUTO_INCREMENT, 
ID_movimiento_caja VARCHAR (1) NOT NULL, 
ID_Tienda INT NOT NULL,
ID_Usuario INT NOT NULL,
fecha DATE, 
Total_movimiento DOUBLE NOT NULL DEFAULT 0, 
anulado BOOLEAN, 
PRIMARY KEY (ID_Comprobante),
FOREIGN KEY (ID_Usuario) REFERENCES usuarios(ID_Usuario)) ENGINE = InnoDB;


CREATE TABLE personas ( 
ID_Comprobante INT NOT NULL,
Tipo_persona VARCHAR (10) NOT NULL,
ID_Persona INT NOT NULL AUTO_INCREMENT,
cedula_persona INT(20) NOT NULL,
nombre_completo_usuario VARCHAR (50) NOT NULL,
fecha_nacimiento_usuario DATE,
correo	VARCHAR (30) NOT NULL,
activo BOOLEAN DEFAULT FALSE, 
primary key(ID_Persona,cedula_persona),
FOREIGN KEY (ID_Comprobante) REFERENCES comprobantes(ID_Comprobante))ENGINE =Innodb;
